unit PlayoutInterfaceForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, Globals;

type
  TPlayoutInterface = class(TForm)
    PlayoutConnection1: TClientSocket;
    PlayoutConnection2: TClientSocket;
    PlayoutConnection3: TClientSocket;
    PlayoutConnection4: TClientSocket;
    PlayoutConnection5: TClientSocket;
    PlayoutConnection6: TClientSocket;
    PlayoutConnection7: TClientSocket;
    PlayoutConnection8: TClientSocket;
    PlayoutConnection9: TClientSocket;
    PlayoutConnection10: TClientSocket;
    PlayoutConnection11: TClientSocket;
    PlayoutConnection12: TClientSocket;
    PlayoutConnection13: TClientSocket;
    PlayoutConnection14: TClientSocket;
    PlayoutConnection15: TClientSocket;
    PlayoutConnection16: TClientSocket;
    PlayoutConnection17: TClientSocket;
    PlayoutConnection18: TClientSocket;
    PlayoutConnection19: TClientSocket;
    PlayoutConnection20: TClientSocket;

    //Handler for socket errors
    procedure PlayoutConnection1Error(Sender: TObject;
    Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection2Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection3Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection4Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection5Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection6Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection7Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection8Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection9Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection10Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection11Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection12Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection13Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection14Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection15Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection16Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection17Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection18Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection19Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnection20Error(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);



    procedure PlayoutConnection1Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection1Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection2Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection2Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection3Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection3Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection4Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection4Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection5Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection5Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection6Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection6Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection7Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection7Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection8Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection8Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection9Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection9Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection10Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection10Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection1Read(Sender: TObject;
      Socket: TCustomWinSocket);
    function ParseReceivedData(CommandIn: String): CommandRec;
    procedure PlayoutConnection2Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection3Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection4Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection5Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection6Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection7Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection8Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection9Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection10Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ResetError(ChannelID: SmallInt);
    procedure PlayoutConnection11Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection11Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection12Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection12Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection13Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection13Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection14Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection14Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection15Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection15Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection16Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection16Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection17Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection17Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection18Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection18Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection19Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection19Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection20Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection20Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);

    procedure ProcessCommandFromController(StationID: SmallInt; Data: String);
    procedure PlayoutConnection11Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection12Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection13Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection14Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection15Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection16Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection17Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection18Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection19Read(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnection20Read(Sender: TObject;
      Socket: TCustomWinSocket);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PlayoutInterface: TPlayoutInterface;

const
  SOH = #1; //Start of heading
  ETX = #3; //End of text
  EOT = #4; //End of transmission

implementation

uses Main;

{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR SOCKET CONNECT/DISCONNECT
////////////////////////////////////////////////////////////////////////////////
//Playout #1
procedure TPlayoutInterface.PlayoutConnection1Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[1].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection1Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[1].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection1Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[1].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[1].ErrorFlag := TRUE;
  //Log error

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #2
procedure TPlayoutInterface.PlayoutConnection2Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[2].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection2Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[2].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection2Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[2].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[2].ErrorFlag := TRUE;
  //Log error

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #3
procedure TPlayoutInterface.PlayoutConnection3Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[3].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection3Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[3].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection3Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[3].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[3].ErrorFlag := TRUE;

  ErrorCode := 0;
end;

//Playout #4
procedure TPlayoutInterface.PlayoutConnection4Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[4].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection4Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[4].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection4Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[4].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[4].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #5
procedure TPlayoutInterface.PlayoutConnection5Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[5].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection5Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[5].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection5Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[5].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[5].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #6
procedure TPlayoutInterface.PlayoutConnection6Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[6].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection6Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[6].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection6Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[6].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[6].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #7
procedure TPlayoutInterface.PlayoutConnection7Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[7].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection7Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[7].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection7Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[7].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[7].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #8
procedure TPlayoutInterface.PlayoutConnection8Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[8].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection8Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[8].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection8Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[8].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[8].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #9
procedure TPlayoutInterface.PlayoutConnection9Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[9].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection9Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[9].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection9Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[9].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[9].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #10
procedure TPlayoutInterface.PlayoutConnection10Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[10].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection10Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[10].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection10Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[10].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[10].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #11
procedure TPlayoutInterface.PlayoutConnection11Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[11].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection11Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[11].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection11Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[11].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[11].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #12
procedure TPlayoutInterface.PlayoutConnection12Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[12].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection12Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[12].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection12Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[12].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[12].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #13
procedure TPlayoutInterface.PlayoutConnection13Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[13].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection13Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[13].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection13Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[13].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[13].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #14
procedure TPlayoutInterface.PlayoutConnection14Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[14].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection14Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[14].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection14Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[14].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[14].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #15
procedure TPlayoutInterface.PlayoutConnection15Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[15].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection15Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[15].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection15Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[15].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[15].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #16
procedure TPlayoutInterface.PlayoutConnection16Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[16].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection16Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[16].Connected := FALSE;
end;

procedure TPlayoutInterface.PlayoutConnection16Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[16].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[16].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #17
procedure TPlayoutInterface.PlayoutConnection17Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[17].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection17Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[17].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection17Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[17].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[17].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #18
procedure TPlayoutInterface.PlayoutConnection18Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[18].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection18Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[18].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection18Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[18].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[18].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #19
procedure TPlayoutInterface.PlayoutConnection19Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[19].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection19Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[19].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection19Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[19].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[19].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//Playout #20
procedure TPlayoutInterface.PlayoutConnection20Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Set Connected flag
  PlayoutControllerInfo[20].Connected := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnection20Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Clear Connected flag
  PlayoutControllerInfo[20].Connected := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnection20Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo[20].Connected := FALSE;
  //Set error indicator
  PlayoutControllerInfo[20].ErrorFlag := TRUE;

  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR RECEIVING AND PROCESSING COMMANDS FROM PLAYOUT CONTROLLERS
////////////////////////////////////////////////////////////////////////////////
//General function to parse data received from Playout Controller
function TPlayoutInterface.ParseReceivedData(CommandIn: String): CommandRec;
var
  Tokens: Array[1..7] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  OutRec: CommandRec;
begin
  try
    //Parse the string for the command and any parameters
    //First, check for valid command framing; should contain at least one SOH, one ETX and one EOT;
    //If not, send back error code 1 = Invalid command syntax received
    //Need to at least have framing characters; also check for SOH at start and EOT at end
    if (Length(CommandIn) > 3) AND (CommandIn[1] = SOH) AND (CommandIn[Length(CommandIn)] = EOT) then
    begin
      //Strip off SOT and EOT
      CommandIn := Copy (CommandIn, 2, Length(CommandIn)-2);
      //Parse for tokens
      for i := 1 to 7 do Tokens[i] := '';
      Cursor := 1;
      TokenIndex :=1;
      repeat
        if (CommandIn[Cursor] = ETX) then
        begin
          Inc(Cursor);
          Inc(TokenIndex);
        end;
        Tokens[TokenIndex] := Tokens[TokenIndex] + CommandIn[Cursor];
        Inc(Cursor);
      until (Cursor > Length(CommandIn));
    end;
    //Set output record
    OutRec.Command := Tokens[1];
    OutRec.Param[1] := Tokens[2];
    OutRec.Param[2] := Tokens[3];
    OutRec.Param[3] := Tokens[4];
    OutRec.Param[4] := Tokens[5];
    OutRec.Param[5] := Tokens[6];
    OutRec.Param[6] := Tokens[7];
    ParseReceivedData := OutRec;
  except
    MainForm.WriteToErrorLog('Error occurred while trying to parse data received from Playout Controller');
  end;
end;

//Generic function to process a string received from a controller
procedure TPlayoutInterface.ProcessCommandFromController(StationID: SmallInt; Data: String);
var
  EOTLocation: SmallInt;
  CommandStr: String;
  CommandData: CommandRec;
  AllCommandsProcessed: Boolean;
begin
  try
    //Process commands
    if (PlayoutControllerInfo[StationID].Connected) then
    begin
      //Init
      AllCommandsProcessed := FALSE;
      //Process commands
      if (Data[1] = SOH) AND (Data[Length(Data)] = EOT) then
      repeat
        //Copy command from data string & parse
        //Find end of command
        EOTLocation := Pos(EOT, Data);
        //Copy from start to end of command & parse
        CommandStr := Copy(Data, 1, EOTLocation);
        CommandData := ParseReceivedData(CommandStr);
        //Delete command from data string for next iteration
        if (EOTLocation < Length(Data)) then
          Data := Copy(Data, EOTLocation+1, Length(Data))
        else
          AllCommandsProcessed := TRUE;
        //Check for automation mode command
        if (CommandData.Command = 'STATUS') then
        begin
          //Param[1] = Error Flag
          if (CommandData.Param[1] = '1') then
            PlayoutControllerInfo[StationID].ErrorFlag := TRUE
          else
            PlayoutControllerInfo[StationID].ErrorFlag := FALSE;
          //Param[2] = Engine OK
          if (CommandData.Param[2] = '1') then
            PlayoutControllerInfo[StationID].TickerEngineOK := TRUE
          else
            PlayoutControllerInfo[StationID].TickerEngineOK := FALSE;
          //Param[3] = Ticker Running
          if (CommandData.Param[3] = '1') then
            PlayoutControllerInfo[StationID].TickerRunning := TRUE
          else
            PlayoutControllerInfo[StationID].TickerRunning := FALSE;
          //Param[4] = Schedule Monitoring Enabled
          if (CommandData.Param[4] = '1') then
            PlayoutControllerInfo[StationID].InManualMode := TRUE
          else
            PlayoutControllerInfo[StationID].InManualMode := FALSE;
          //Param[5] = Ticker Playlist Name
          PlayoutControllerInfo[StationID].CurrentPlaylistName := CommandData.Param[5];
        end;
      until (AllCommandsProcessed);
    end;
  except
    //MainForm.WriteToErrorLog('Error occurred while trying to process parsed data received from Playout Controller #1');
  end;
end;

//Data packet from Controller #1
procedure TPlayoutInterface.PlayoutConnection1Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(1, Data);
end;

//Data packet from Controller #2
procedure TPlayoutInterface.PlayoutConnection2Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(2, Data);
end;

//Data packet from Controller #3
procedure TPlayoutInterface.PlayoutConnection3Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(3, Data);
end;

//Data packet from Controller #4
procedure TPlayoutInterface.PlayoutConnection4Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(4, Data);
end;

//Data packet from Controller #5
procedure TPlayoutInterface.PlayoutConnection5Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(5, Data);
end;

//Data packet from Controller #6
procedure TPlayoutInterface.PlayoutConnection6Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(6, Data);
end;

//Data packet from Controller #7
procedure TPlayoutInterface.PlayoutConnection7Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(7, Data);
end;

//Data packet from Controller #8
procedure TPlayoutInterface.PlayoutConnection8Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(8, Data);
end;

//Data packet from Controller #9
procedure TPlayoutInterface.PlayoutConnection9Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(9, Data);
end;

//Data packet from Controller #10
procedure TPlayoutInterface.PlayoutConnection10Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(10, Data);
end;

//Data packet from Controller #11
procedure TPlayoutInterface.PlayoutConnection11Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(11, Data);
end;

//Data packet from Controller #12
procedure TPlayoutInterface.PlayoutConnection12Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(12, Data);
end;

//Data packet from Controller #13
procedure TPlayoutInterface.PlayoutConnection13Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(13, Data);
end;

//Data packet from Controller #14
procedure TPlayoutInterface.PlayoutConnection14Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(14, Data);
end;

//Data packet from Controller #15
procedure TPlayoutInterface.PlayoutConnection15Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(15, Data);
end;

//Data packet from Controller #16
procedure TPlayoutInterface.PlayoutConnection16Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(16, Data);
end;

//Data packet from Controller #17
procedure TPlayoutInterface.PlayoutConnection17Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(17, Data);
end;

//Data packet from Controller #18
procedure TPlayoutInterface.PlayoutConnection18Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(18, Data);
end;

//Data packet from Controller #19
procedure TPlayoutInterface.PlayoutConnection19Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(19, Data);
end;

//Data packet from Controller #20
procedure TPlayoutInterface.PlayoutConnection20Read(Sender: TObject;
  Socket: TCustomWinSocket);
var
  Data: String;
begin
  Data := Socket.ReceiveText;
  ProcessCommandFromController(20, Data);
end;

//Function to send reset error command to Playout Controller
procedure TPlayoutInterface.ResetError(ChannelID: SmallInt);
var
  CmdStr: String;
  ParamVal: SmallInt;
begin
  try
    CmdStr := SOH + 'RESET_ERROR' + EOT;
    PlayoutConnection1.Socket.SendText(CmdStr);
  except
    MainForm.WriteToErrorLog('Error occurred while trying to reset error on Playout Controller #' + IntToStr(ChannelID));
  end;
end;

end.
