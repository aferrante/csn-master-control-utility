unit Globals;

interface

uses
  StBase, StColl;

{$H+}
type
  NetworkRec = record
    NetworkActive: Boolean;
    StationGroupID: SmallInt;
    StationGroupName: String[25];
    DBConnectionString: String[250];
  end;

  PlayoutControllerRec = Record
    ControllerActive: Boolean;
    StationIndex: SmallInt;
    StationID: SmallInt;
    StationDescription: String[20];
    StationGroupID: SmallInt;
    GroupStationIndex: SmallInt;
    StationGroupName: String[50];
    Enabled: Boolean;
    EnableForMasterControl: Boolean;
    IPAddress: String[20];
    Port: Integer;
    Connected: Boolean;
    TickerEngineOK: Boolean;
    TickerRunning: Boolean;
    InManualMode: Boolean;
    ErrorFlag: Boolean;
    ErrorCode: SmallInt;
    CurrentPlaylistName: String[100];
  end;

  CommandRec = record
    Command: String;
    Param: Array[1..6] of String;
  end;

var
  PrefsFile: TextFile;
  Description: String;
  NetworkInfo: Array[1..10] of NetworkRec; //Array for networks
  NetworkCount: SmallInt;
  PlayoutControllerInfo: Array[1..20] of PlayoutControllerRec; //Array for Playout Controller Stations
  PlayoutControllerCount: SmallInt;

  DBConnectionString_X: String;
  DBConnectionString_Y: String;
  DBServerXEnabled: Boolean;
  DBServerYEnabled: Boolean;
  UseXAsMainDBServer: Boolean;

  SocketConnected: Boolean;

  ErrorLogFile: TextFile; {File for logging hardware interface errors}
  Error_Condition: Boolean; {Flag to indicate an error occurred}
  ErrorLoggingEnabled: Boolean; {Flag to indicate error logging is enabled}

  CommandLoggingEnabled: Boolean; {Flag to indicate command logging is enabled}
  CommandLogFile: TextFile; {File for logging commands}
  CommandLogFileDirectoryPath: String;

  PacketEnable: Boolean;

  LogFilePath: String;
  LogFileName: String;

  ScheduleMonitoringEnabled: Boolean;

  Startup: Boolean;

const
  SCHEDULE_MODE = 0;
  MANUAL_MODE = 1;
  X_AND_Y = 0;
  X_ONLY = 1;
  Y_ONLY = 2;
  LOG_SELECT_X = 0;
  LOG_SELECT_Y = 1;

implementation

end.
