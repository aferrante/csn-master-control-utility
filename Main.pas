////////////////////////////////////////////////////////////////////////////////
// Master Control  application for Comcast/NBC Ticker
// V1.0.0  07/29/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Misc. enhancements prior to launch
// V1.0.3  08/07/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support to show playlists loaded on both X & Y sides at all times.
// V1.0.4  08/12/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed bug with reporting of scheduled/manual mode on Y-Side.
// V1.0.5  08/28/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  Grids_ts, TSGrid, TSDBGrid, DBCtrls, StColl, MPlayer, OleCtrls,
  OleServer, Grids, PBJustOne, ClipBrd, TSMask, INIFiles, DBGrids, OoMisc, AdStatLt,
  Globals, AdPacket, AdPort, PlayoutInterfaceForm, Tabs, SUIComboBox,
  SUIButton, SUIMainMenu, SUIForm, TSImageList, SUIDBCtrls, SUIImagePanel,
  SUIGroupBox, SUIRadioGroup;

{H+} //ANSI strings
//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    tsImageList: TtsImageList;
    tsImageList1: TtsImageList;
    tsImageList2: TtsImageList;
    tsMaskDefs1: TtsMaskDefs;
    PBJustOne1: TPBJustOne;
    Panel8: TPanel;
    Image2: TImage;
    Panel3: TPanel;
    DatabaseDisconnectLED_X: TApdStatusLight;
    DatabaseConnectLED_X: TApdStatusLight;
    DateLabel: TLabel;
    TimeLabel: TLabel;
    StaticText17: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    DatabaseDisconnectLED_Y: TApdStatusLight;
    DatabaseConnectLED_Y: TApdStatusLight;
    ErrorFlashTimer: TTimer;
    TimeOfDayTimer: TTimer;
    LabelX: TStaticText;
    StaticText1: TStaticText;
    N1: TMenuItem;
    E1: TMenuItem;
    N2: TMenuItem;
    S1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    MainMenu: TMainMenu;
    Panel1: TPanel;
    StatusBar: TStatusBar;
    Panel2: TPanel;
    ErrorLogViewerPnl: TPanel;
    StaticText25: TStaticText;
    StaticText21: TStaticText;
    LogViewerLabel: TStaticText;
    NetworkSelectDropDown: TsuiComboBox;
    StaticText27: TStaticText;
    TriggerPlaylistBtn: TsuiButton;
    PausePlaylistBtn: TsuiButton;
    ResetPlaylistBtn: TsuiButton;
    ResetErrorBtn: TsuiButton;
    Panel5: TPanel;
    GraphicsEngineLED_X: TApdStatusLight;
    StaticText28: TStaticText;
    StaticText29: TStaticText;
    TickerRunningTxt: TStaticText;
    TickerRunningLED_X: TApdStatusLight;
    CurrentPlaylistName_X: TLabel;
    StaticText34: TStaticText;
    AirChainErrorLED_X: TApdStatusLight;
    StaticText31: TStaticText;
    SchedulingModeBtn: TsuiButton;
    ScheduleModeLED_X: TApdStatusLight;
    ScheduleModeTxt: TStaticText;
    ManualModeLED_X: TApdStatusLight;
    ManualModeTxt: TStaticText;
    SelectPlaylistBtn: TsuiButton;
    PlaylistSelectPanel: TPanel;
    AvailablePlaylistGrid: TtsDBGrid;
    LoadPlaylistBtn: TsuiButton;
    CancelPlaylistLoadBtn: TsuiButton;
    ErrorLogGrid: TtsDBGrid;
    ErrorLogNavigator: TsuiDBNavigator;
    RefreshErrorLogBtn: TsuiButton;
    ViewErrorLogBtn: TsuiButton;
    GraphicsEngineLED_Y: TApdStatusLight;
    TickerRunningLED_Y: TApdStatusLight;
    AirChainErrorLED_Y: TApdStatusLight;
    ScheduleModeLED_Y: TApdStatusLight;
    ManualModeLED_Y: TApdStatusLight;
    AirChainSelectRadioBtn: TsuiRadioGroup;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    N5: TMenuItem;
    UseXSideServerasMaster: TMenuItem;
    UseYSideServerasMaster: TMenuItem;
    N6: TMenuItem;
    XSideServerEnabled: TMenuItem;
    YSideServerEnabled: TMenuItem;
    NetworkPnl_2: TPanel;
    AirChainStatusLED_3: TApdStatusLight;
    AirChainStatusLED_4: TApdStatusLight;
    AirChainConnectLED_3: TApdStatusLight;
    AirChainConnectLED_4: TApdStatusLight;
    AirChainDescription_3: TStaticText;
    AirChainDescription_4: TStaticText;
    NetworkPnl_3: TPanel;
    AirChainStatusLED_5: TApdStatusLight;
    AirChainStatusLED_6: TApdStatusLight;
    AirChainConnectLED_5: TApdStatusLight;
    AirChainConnectLED_6: TApdStatusLight;
    AirChainDescription_5: TStaticText;
    AirChainDescription_6: TStaticText;
    NetworkPnl_4: TPanel;
    AirChainStatusLED_7: TApdStatusLight;
    AirChainStatusLED_8: TApdStatusLight;
    AirChainConnectLED_7: TApdStatusLight;
    AirChainConnectLED_8: TApdStatusLight;
    AirChainDescription_7: TStaticText;
    AirChainDescription_8: TStaticText;
    NetworkPnl_5: TPanel;
    AirChainStatusLED_9: TApdStatusLight;
    AirChainStatusLED_10: TApdStatusLight;
    AirChainConnectLED_9: TApdStatusLight;
    AirChainConnectLED_10: TApdStatusLight;
    AirChainDescription_9: TStaticText;
    AirChainDescription_10: TStaticText;
    NetworkPnl_6: TPanel;
    AirChainStatusLED_11: TApdStatusLight;
    AirChainStatusLED_12: TApdStatusLight;
    AirChainConnectLED_11: TApdStatusLight;
    AirChainConnectLED_12: TApdStatusLight;
    AirChainDescription_11: TStaticText;
    AirChainDescription_12: TStaticText;
    NetworkPnl_7: TPanel;
    AirChainStatusLED_13: TApdStatusLight;
    AirChainStatusLED_14: TApdStatusLight;
    AirChainConnectLED_13: TApdStatusLight;
    AirChainConnectLED_14: TApdStatusLight;
    AirChainDescription_13: TStaticText;
    AirChainDescription_14: TStaticText;
    NetworkPnl_1: TPanel;
    AirChainStatusLED_1: TApdStatusLight;
    AirChainStatusLED_2: TApdStatusLight;
    AirChainConnectLED_1: TApdStatusLight;
    AirChainConnectLED_2: TApdStatusLight;
    AirChainDescription_1: TStaticText;
    AirChainDescription_2: TStaticText;
    NetworkPnl_8: TPanel;
    AirChainStatusLED_15: TApdStatusLight;
    AirChainStatusLED_16: TApdStatusLight;
    AirChainConnectLED_15: TApdStatusLight;
    AirChainConnectLED_16: TApdStatusLight;
    AirChainDescription_15: TStaticText;
    AirChainDescription_16: TStaticText;
    NetworkPnl_9: TPanel;
    AirChainStatusLED_17: TApdStatusLight;
    AirChainStatusLED_18: TApdStatusLight;
    AirChainConnectLED_17: TApdStatusLight;
    AirChainConnectLED_18: TApdStatusLight;
    AirChainDescription_17: TStaticText;
    AirChainDescription_18: TStaticText;
    NetworkPnl_10: TPanel;
    AirChainStatusLED_19: TApdStatusLight;
    AirChainStatusLED_20: TApdStatusLight;
    AirChainConnectLED_19: TApdStatusLight;
    AirChainConnectLED_20: TApdStatusLight;
    AirChainDescription_19: TStaticText;
    AirChainDescription_20: TStaticText;
    AirChainSelectErrorLog: TsuiRadioGroup;
    CloseErrorLogBtn: TsuiButton;
    ViewAsRunLogBtn: TsuiButton;
    AsRunLogViewerPnl: TPanel;
    StaticText8: TStaticText;
    AsRunLogGrid: TtsDBGrid;
    AsRunLogNavigator: TsuiDBNavigator;
    RefreshAsRunLogBtn: TsuiButton;
    AirChainSelectAsRunLog: TsuiRadioGroup;
    CloseAsRunLogBtn: TsuiButton;
    YDBLabel: TStaticText;
    N7: TMenuItem;
    R1: TMenuItem;
    CurrentPlaylistName_Y: TLabel;
    StaticText9: TStaticText;
    StaticText10: TStaticText;

    //General program functions
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StorePrefs;
    procedure LoadPrefs;

    //Dialog functions
    procedure StorePrefs1Click(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure S1Click(Sender: TObject);

    //Playlist operations
    //Utility functions
    procedure ReloadDataFromDatabase;

    //Error/log functions
    procedure DatabaseConnectError(ExceptionString: String);
    procedure DatabaseEngineError(ExceptionString: String);

    procedure WriteToErrorLog (ErrorString: String);

    //Timer functions
    procedure ErrorFlashTimerTimer(Sender: TObject);
    procedure ResetError1Click(Sender: TObject);
    procedure TimeOfDayTimerTimer(Sender: TObject);

    //GUI event handlers
    procedure SchedulingModeBtnClick(Sender: TObject);
    procedure TriggerPlaylistBtnClick(Sender: TObject);
    procedure PausePlaylistBtnClick(Sender: TObject);
    procedure ResetPlaylistBtnClick(Sender: TObject);
    procedure ResetErrorBtnClick(Sender: TObject);
    procedure SelectPlaylistBtnClick(Sender: TObject);
    procedure LoadPlaylistBtnClick(Sender: TObject);
    procedure CancelPlaylistLoadBtnClick(Sender: TObject);
    procedure AvailablePlaylistGridDblClick(Sender: TObject);
    procedure NetworkSelectDropDownChange(Sender: TObject);
    procedure CloseErrorLogBtnClick(Sender: TObject);
    procedure ViewErrorLogBtnClick(Sender: TObject);
    procedure RefreshErrorLogBtnClick(Sender: TObject);

    //Functions for connection and status
    procedure DisplayEnabledAirChains;
    procedure UpdateStatusAllAirChains;
    procedure ConnectToControllers;
    procedure UpdateMainControlPanel;
    procedure SendCommandToController(CommandStr: String);
    procedure UpdateAsRunLog;
    procedure UpdateErrorLog;
    procedure ProcessNetworkSelectChange;
    procedure ViewAsRunLogBtnClick(Sender: TObject);
    procedure CloseAsRunLogBtnClick(Sender: TObject);
    procedure RefreshAsRunLogBtnClick(Sender: TObject);
    procedure AirChainSelectAsRunLogClick(Sender: TObject);
    procedure AirChainSelectErrorLogClick(Sender: TObject);
    procedure UseXSideServerasMasterClick(Sender: TObject);
    procedure XSideServerEnabledClick(Sender: TObject);
    procedure YSideServerEnabledClick(Sender: TObject);
    procedure UseYSideServerasMasterClick(Sender: TObject);
    procedure AirChainSelectRadioBtnClick(Sender: TObject);
    procedure R1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule;      //Data module


{$R *.DFM}
{$H+}

//Procedures to delete data pointers in each collection node
procedure Playout_Controller_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PlayoutControllerRec));
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for main form creation
procedure TMainForm.FormCreate(Sender: TObject);
begin
  //Inits
  Startup := TRUE;
  Error_Condition := FALSE;

  //oolean to indciate error logging is enabled; default = false; will be
  //enabled after one second timer expires
  ErrorLoggingEnabled := True;
end;

//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
var
  i: SmallInt;
  PlayoutControllerRecPtr: ^PlayoutControllerRec;
begin
  if (Startup = True) then
  begin
    //Init flags
    Startup := False;

    //Clear error flags
    for i := 1 to 20 do PlayoutControllerInfo[i].ErrorFlag := FALSE;

    LogFilePath := 'C:\Logfiles\';
    LogFileName := LogFilePath + 'ComcastNBC';

    //Load pereferences file
    LoadPrefs;

    //Set state of DB servers
    if (UseXAsMainDBServer) then
    begin
      UseXSideServerasMaster.Checked := TRUE;
      UseYSideServerasMaster.Checked := FALSE;
      YDBLabel.Visible := FALSE;
    end
    else begin
      UseXSideServerasMaster.Checked := FALSE;
      UseYSideServerasMaster.Checked := TRUE;
      YDBLabel.Visible := TRUE;
    end;

    //Connect to X Database
    if (DBServerXEnabled) then
    try
      XSideServerEnabled.Checked := TRUE;
      With dmMain do
      begin
        dbMasterControl_X.Connected := FALSE;
        dbMasterControl_X.ConnectionString := DBConnectionString_X;
        dbMasterControl_X.Connected := TRUE;
        //Set indicators
        if (dbMasterControl_X.Connected) then
        begin
          DatabaseConnectLED_X.Lit := TRUE;
          DatabaseDisconnectLED_X.Lit := FALSE;
        end
        else begin
          DatabaseConnectLED_X.Lit := FALSE;
          DatabaseDisconnectLED_X.Lit := TRUE;
        end;
      end;
    except
      //Set indicators
      XSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_X.Lit := TRUE;
      DatabaseConnectLED_X.Lit := FALSE;
    end
    else begin
      XSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_X.Lit := TRUE;
      DatabaseConnectLED_X.Lit := FALSE;
    end;

    //Connect to Y Database
    if (DBServerYEnabled) then
    try
      YSideServerEnabled.Checked := TRUE;
      With dmMain do
      begin
        dbMasterControl_Y.Connected := FALSE;
        dbMasterControl_Y.ConnectionString := DBConnectionString_Y;
        dbMasterControl_Y.Connected := TRUE;
        //Set indicators
        if (dbMasterControl_Y.Connected) then
        begin
          DatabaseConnectLED_Y.Lit := TRUE;
          DatabaseDisconnectLED_Y.Lit := FALSE;
        end
        else begin
          DatabaseConnectLED_Y.Lit := FALSE;
          DatabaseDisconnectLED_Y.Lit := TRUE;
        end;
      end;
    except
      //Set indicators
      YSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_Y.Lit := TRUE;
      DatabaseConnectLED_Y.Lit := FALSE;
    end
    else begin
      YSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_Y.Lit := TRUE;
      DatabaseConnectLED_Y.Lit := FALSE;
    end;

    //Set radio buton accordingly if only one DB connected
    if (dmMain.dbMasterControl_X.Connected) and not (dmMain.dbMasterControl_Y.Connected) then
    begin
      AirChainSelectRadioBtn.ItemIndex := X_ONLY;
      AirChainSelectErrorLog.ItemIndex := LOG_SELECT_X;
      AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_X;
    end
    else if (dmMain.dbMasterControl_Y.Connected) and not (dmMain.dbMasterControl_X.Connected) then
    begin
      AirChainSelectRadioBtn.ItemIndex := Y_ONLY;
      AirChainSelectErrorLog.ItemIndex := LOG_SELECT_Y;
      AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_Y;
    end;

    //Init
    for i := 1 to 20 do PlayoutControllerInfo[i].Connected := FALSE;

    //Call procedure to load data from database
    ReloadDataFromDatabase;

    //Connect to the Controllers
    ConnectToControllers;

    //Setup the UI to show enabled air-chains
    DisplayEnabledAirChains;

    //Init to first network
    NetworkSelectDropDown.ItemIndex := 0;
    ProcessNetworkSelectChange;
  end;
end;

//Handler for program exit from main menu
procedure TMainForm.E1Click(Sender: TObject);
begin
  Close;
end;

//Handler for reset error condition
procedure TMainForm.ResetError1Click(Sender: TObject);
var
  i: SmallInt;
begin
  //Send reset error command to appropriate Playout Controller
  for i := 1 to 10 do
    if (PlayoutControllerInfo[i].ErrorFlag = TRUE) then
      PlayoutInterface.ResetError(i);
  //Clear error flags & indicators
  for i := 1 to 10 do PlayoutControllerInfo[i].ErrorFlag := FALSE;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the Comcast/NBC Ticker Master Control application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Store preferences
    StorePrefs;
    //Set action for main form
    Action := caFree;
    //Disable timers
    ErrorFlashTimer.Enabled := FALSE;
    TimeOfDayTimer.Enabled := FALSE;
  end
  else
     Action := caNone;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs1Click(Sender: TObject);
begin
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING VARIOUS PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.N4Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.S1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.DBString_X.Text := DBConnectionString_X;
    Modal.DBString_Y.Text := DBConnectionString_Y;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      DBConnectionString_X := Modal.DBString_X.Text;
      DBConnectionString_Y := Modal.DBString_Y.Text;
      try
        //Activate database & tables
        With dmMain do
        begin
          dbMasterControl_X.Connected := FALSE;
          dbMasterControl_X.ConnectionString := DBConnectionString_X;
          dbMasterControl_X.Connected := TRUE;

          dbMasterControl_Y.Connected := FALSE;
          dbMasterControl_Y.ConnectionString := DBConnectionString_Y;
          dbMasterControl_Y.Connected := TRUE;

        end;
        //Store the preferences
        StorePrefs;
      except
        MessageDlg('Error occurred while trying to set user preferences!', mtError, [mbOK], 0);
      end;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  i: SmallInt;
  PlayoutControllerRecPtr: ^PlayoutControllerRec;
begin
  try
    //Load in the names of the playout controller groups
    if (UseXAsMainDBServer) then
      dmMain.MCQuery.Connection := dmMain.dbMasterControl_X
    else
      dmMain.MCQuery.Connection := dmMain.dbMasterControl_Y;

     //Init from the database
    with dmMain.MCQuery do
    begin

      //Load in the data from the sponsor logos table; iterate for all records in table
      for i := 1 to 20 do NetworkInfo[i].NetworkActive := FALSE;
      
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM Playout_Station_Groups');
      Open;
      if (RecordCount > 0) then
      begin
        First;
        i := 1;
        repeat
          With NetworkInfo[i] do
          begin
            NetworkActive := TRUE;
            StationGroupID := FieldByName('StationGroupID').AsInteger;
            StationGroupName := FieldByName('StationGroupName').AsString;
            DBConnectionString := FieldByName('DBConnectionString').AsString;

            NetworkCount := i;

            networkSelectDropDown.Items.Add(StationGroupName);
          end;
          inc (i);
          //Go to next record
          Next;
        until (i > 10) OR (EOF); //Repeat until end of dataset
        NetworkSelectDropDown.ItemIndex := 0;
      end;
      //Close query
      Close;

      //Load in the data from the sponsor logos table; iterate for all records in table
      for i := 1 to 20 do PlayoutControllerInfo[i].ControllerActive := FALSE;

      SQL.Clear;
      SQL.Add('SELECT * FROM Playout_Station_IDs');
      Open;
      if (RecordCount > 0) then
      begin
        First;
        i := 1;
        repeat
          //Add item to scripts collection
          With PlayoutControllerInfo[i] do
          begin
            ControllerActive := TRUE;
            StationIndex := FieldByName('StationIndex').AsInteger;
            StationID := FieldByName('StationID').AsInteger;
            StationDescription := FieldByName('StationDescription').AsString;
            StationGroupID := FieldByName('StationGroupID').AsInteger;
            GroupStationIndex := FieldByName('GroupStationIndex').AsInteger;
            StationGroupName := FieldByName('StationGroupName').AsString;
            Enabled := FieldByName('Enabled').AsBoolean;
            EnableForMasterControl := FieldByName('EnableForMasterControl').AsBoolean;
            IPAddress := FieldByName('IPAddress').AsString;
            Port := FieldByName('Port').AsInteger;
            //Init
            Connected := FALSE;
            TickerEngineOK := FALSE;
            TickerRunning := FALSE;
            InManualMode := FALSE;
            ErrorFlag := FALSE;
            ErrorCode := 0;
            CurrentPlaylistName := '';
            PlayoutControllerCount := i;
          end;
          inc(i);
          //Go to next record
          Next;
        until (i > 20) OR (EOF); //Repeat until end of dataset
      end;
      //Close query
      Close;
    end;
  except
    WriteToErrorLog('Error occurred while trying to reload data from database');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TRIGGER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to set the selected station Group ID when the Network Select tab is changed


////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database connection problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseConnectError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    DatabaseDisconnectLED_X.Lit := TRUE;
    DatabaseConnectLED_X.Lit := FALSE;

    //WriteToErrorLog procedure defined in Engine Interface unit
    WriteToErrorLog (ExceptionString);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database engine problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseEngineError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    DatabaseDisconnectLED_X.Lit := TRUE;
    DatabaseConnectLED_X.Lit := FALSE;

    //WriteToErrorLog procedure defined in Engine Interface unit
    WriteToErrorLog (ExceptionString);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES TO LOAD & STORE PREFS HERE BEACAUSE OF WEIRD DEBUGGER ISSUES -
// BREAKPOINTS NOT IN CORRECT PLACE
////////////////////////////////////////////////////////////////////////////////
//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Master_Control.ini');
    //Database connection
    DBConnectionString_X := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String (X)',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1;Initial Catalog=CSN-MASTER-CONTROL;Data Source=OWNER-PC\SQLEXPRESS');
    DBConnectionString_Y := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String (Y)',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1;Initial Catalog=CSN-MASTER-CONTROL;Data Source=OWNER-PC\SQLEXPRESS');
    DBServerXEnabled := PrefsINI.ReadBool('Database Connections', '"X" Side Database Server Enabled', TRUE);
    DBServerYEnabled := PrefsINI.ReadBool('Database Connections', '"Y" Side Database Server Enabled', TRUE);
    UseXAsMainDBServer := PrefsINI.ReadBool('Database Connections', 'Use "X" Side as Main Database Server', TRUE);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store usee preferences
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Master_Control.ini');
    //Save the settings
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String (X)', DBConnectionString_X);
    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String (Y)', DBConnectionString_Y);
    PrefsINI.WriteBool('Database Connections', '"X" Side Database Server Enabled', DBServerXEnabled);
    PrefsINI.WriteBool('Database Connections', '"Y" Side Database Server Enabled', DBServerYEnabled);
    PrefsINI.WriteBool('Database Connections', 'Use "X" Side as Main Database Server', UseXAsMainDBServer);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Handler for Error Flash timer
procedure TMainForm.ErrorFlashTimerTimer(Sender: TObject);
var
  i: SmallInt;
  ErrorFound: Boolean;
begin
  //See if all errors have been reset
  ErrorFound := FALSE;
  for i := 1 to 10 do
  begin
    if (PlayoutControllerInfo[i].ErrorFlag = TRUE) then ErrorFound := TRUE;
  end;
  //Flag if error found
  if (ErrorFound) then
  begin
    //Do main error panel
    //if (ErrorPanel.Visible) then ErrorPanel.Visible := FALSE
    //else ErrorPanel.Visible := TRUE;

    //Do LEDs
    //if (PlayoutControllerInfo[1].ErrorFlag = TRUE) then
    //begin
    //  if (ErrorLED1.Lit = TRUE) then ErrorLED1.Lit := FALSE
    //  else ErrorLED1.Lit := TRUE;
    //end;
  //Otherwise, reset
  end
  else begin
  {
    ErrorFlashTimer.Enabled := FALSE;
    ErrorPanel.Visible := FALSE;
    ErrorLED1.Lit := FALSE;
    ErrorLED2.Lit := FALSE;
    ErrorLED3.Lit := FALSE;
    ErrorLED4.Lit := FALSE;
    ErrorLED5.Lit := FALSE;
    ErrorLED6.Lit := FALSE;
    ErrorLED7.Lit := FALSE;
    ErrorLED8.Lit := FALSE;
  }
  end;
end;

//Procedure to display enabled air-chains
procedure TMainForm.DisplayEnabledAirChains;
begin
  //Do network #1
  if (NetworkInfo[1].NetworkActive) then
  begin
    NetworkPnl_1.Visible := TRUE;
    if (PlayoutControllerInfo[1].ControllerActive) then
      AirChainDescription_1.Caption := PlayoutControllerInfo[1].StationDescription;
    if (PlayoutControllerInfo[2].ControllerActive) then
      AirChainDescription_2.Caption := PlayoutControllerInfo[2].StationDescription;
  end
  else NetworkPnl_1.Visible := FALSE;
  //Do network #2
  if (NetworkInfo[2].NetworkActive) then
  begin
    NetworkPnl_2.Visible := TRUE;
    if (PlayoutControllerInfo[3].ControllerActive) then
      AirChainDescription_3.Caption := PlayoutControllerInfo[3].StationDescription;
    if (PlayoutControllerInfo[4].ControllerActive) then
      AirChainDescription_4.Caption := PlayoutControllerInfo[4].StationDescription;
  end
  else NetworkPnl_2.Visible := FALSE;
  //Do network #3
  if (NetworkInfo[3].NetworkActive) then
  begin
    NetworkPnl_3.Visible := TRUE;
    if (PlayoutControllerInfo[5].ControllerActive) then
      AirChainDescription_5.Caption := PlayoutControllerInfo[5].StationDescription;
    if (PlayoutControllerInfo[6].ControllerActive) then
      AirChainDescription_6.Caption := PlayoutControllerInfo[6].StationDescription;
  end
  else NetworkPnl_3.Visible := FALSE;
  //Do network #4
  if (NetworkInfo[4].NetworkActive) then
  begin
    NetworkPnl_4.Visible := TRUE;
    if (PlayoutControllerInfo[7].ControllerActive) then
      AirChainDescription_7.Caption := PlayoutControllerInfo[7].StationDescription;
    if (PlayoutControllerInfo[8].ControllerActive) then
      AirChainDescription_8.Caption := PlayoutControllerInfo[8].StationDescription;
  end
  else NetworkPnl_4.Visible := FALSE;
  //Do network #5
  if (NetworkInfo[5].NetworkActive) then
  begin
    NetworkPnl_5.Visible := TRUE;
    if (PlayoutControllerInfo[9].ControllerActive) then
      AirChainDescription_9.Caption := PlayoutControllerInfo[9].StationDescription;
    if (PlayoutControllerInfo[10].ControllerActive) then
      AirChainDescription_10.Caption := PlayoutControllerInfo[10].StationDescription;
  end
  else NetworkPnl_5.Visible := FALSE;
  //Do network #6
  if (NetworkInfo[6].NetworkActive) then
  begin
    NetworkPnl_6.Visible := TRUE;
    if (PlayoutControllerInfo[11].ControllerActive) then
      AirChainDescription_11.Caption := PlayoutControllerInfo[11].StationDescription;
    if (PlayoutControllerInfo[12].ControllerActive) then
      AirChainDescription_12.Caption := PlayoutControllerInfo[12].StationDescription;
  end
  else NetworkPnl_6.Visible := FALSE;
  //Do network #7
  if (NetworkInfo[7].NetworkActive) then
  begin
    NetworkPnl_7.Visible := TRUE;
    if (PlayoutControllerInfo[13].ControllerActive) then
      AirChainDescription_13.Caption := PlayoutControllerInfo[13].StationDescription;
    if (PlayoutControllerInfo[14].ControllerActive) then
      AirChainDescription_14.Caption := PlayoutControllerInfo[14].StationDescription;
  end
  else NetworkPnl_7.Visible := FALSE;
  //Do network #8
  if (NetworkInfo[8].NetworkActive) then
  begin
    NetworkPnl_8.Visible := TRUE;
    if (PlayoutControllerInfo[15].ControllerActive) then
      AirChainDescription_15.Caption := PlayoutControllerInfo[15].StationDescription;
    if (PlayoutControllerInfo[16].ControllerActive) then
      AirChainDescription_16.Caption := PlayoutControllerInfo[16].StationDescription;
  end
  else NetworkPnl_8.Visible := FALSE;
  //Do network #9
  if (NetworkInfo[9].NetworkActive) then
  begin
    NetworkPnl_9.Visible := TRUE;
    if (PlayoutControllerInfo[17].ControllerActive) then
      AirChainDescription_17.Caption := PlayoutControllerInfo[17].StationDescription;
    if (PlayoutControllerInfo[18].ControllerActive) then
      AirChainDescription_18.Caption := PlayoutControllerInfo[18].StationDescription;
  end
  else NetworkPnl_9.Visible := FALSE;
  //Do network #10
  if (NetworkInfo[10].NetworkActive) then
  begin
    NetworkPnl_10.Visible := TRUE;
    if (PlayoutControllerInfo[19].ControllerActive) then
      AirChainDescription_19.Caption := PlayoutControllerInfo[19].StationDescription;
    if (PlayoutControllerInfo[20].ControllerActive) then
      AirChainDescription_20.Caption := PlayoutControllerInfo[20].StationDescription;
  end
  else NetworkPnl_10.Visible := FALSE;
end;

//Procedure to update the status of all of the air-chains
procedure TMainForm.UpdateStatusAllAirChains;
begin
  //Do air-chain #1
  if (PlayoutControllerInfo[1].ControllerActive) then
  begin
    if (PlayoutControllerInfo[1].Connected) then AirChainConnectLED_1.Lit := TRUE
    else AirChainConnectLED_1.Lit := FALSE;
    if (PlayoutControllerInfo[1].ErrorFlag) then AirChainStatusLED_1.Lit := TRUE
    else AirChainStatusLED_1.Lit := FALSE;
  end;
  //Do air-chain #2
  if (PlayoutControllerInfo[2].ControllerActive) then
  begin
    if (PlayoutControllerInfo[2].Connected) then AirChainConnectLED_2.Lit := TRUE
    else AirChainConnectLED_2.Lit := FALSE;
    if (PlayoutControllerInfo[2].ErrorFlag) then AirChainStatusLED_2.Lit := TRUE
    else AirChainStatusLED_2.Lit := FALSE;
  end;
  //Do air-chain #3
  if (PlayoutControllerInfo[3].ControllerActive) then
  begin
    if (PlayoutControllerInfo[3].Connected) then AirChainConnectLED_3.Lit := TRUE
    else AirChainConnectLED_3.Lit := FALSE;
    if (PlayoutControllerInfo[3].ErrorFlag) then AirChainStatusLED_3.Lit := TRUE
    else AirChainStatusLED_3.Lit := FALSE;
  end;
  //Do air-chain #4
  if (PlayoutControllerInfo[4].ControllerActive) then
  begin
    if (PlayoutControllerInfo[4].Connected) then AirChainConnectLED_4.Lit := TRUE
    else AirChainConnectLED_4.Lit := FALSE;
    if (PlayoutControllerInfo[4].ErrorFlag) then AirChainStatusLED_4.Lit := TRUE
    else AirChainStatusLED_4.Lit := FALSE;
  end;
  //Do air-chain #5
  if (PlayoutControllerInfo[5].ControllerActive) then
  begin
    if (PlayoutControllerInfo[5].Connected) then AirChainConnectLED_5.Lit := TRUE
    else AirChainConnectLED_5.Lit := FALSE;
    if (PlayoutControllerInfo[5].ErrorFlag) then AirChainStatusLED_5.Lit := TRUE
    else AirChainStatusLED_5.Lit := FALSE;
  end;
  //Do air-chain #6
  if (PlayoutControllerInfo[6].ControllerActive) then
  begin
    if (PlayoutControllerInfo[6].Connected) then AirChainConnectLED_6.Lit := TRUE
    else AirChainConnectLED_6.Lit := FALSE;
    if (PlayoutControllerInfo[6].ErrorFlag) then AirChainStatusLED_6.Lit := TRUE
    else AirChainStatusLED_6.Lit := FALSE;
  end;
  //Do air-chain #7
  if (PlayoutControllerInfo[7].ControllerActive) then
  begin
    if (PlayoutControllerInfo[7].Connected) then AirChainConnectLED_7.Lit := TRUE
    else AirChainConnectLED_7.Lit := FALSE;
    if (PlayoutControllerInfo[7].ErrorFlag) then AirChainStatusLED_7.Lit := TRUE
    else AirChainStatusLED_7.Lit := FALSE;
  end;
  //Do air-chain #8
  if (PlayoutControllerInfo[8].ControllerActive) then
  begin
    if (PlayoutControllerInfo[8].Connected) then AirChainConnectLED_8.Lit := TRUE
    else AirChainConnectLED_8.Lit := FALSE;
    if (PlayoutControllerInfo[8].ErrorFlag) then AirChainStatusLED_8.Lit := TRUE
    else AirChainStatusLED_8.Lit := FALSE;
  end;
  //Do air-chain #9
  if (PlayoutControllerInfo[9].ControllerActive) then
  begin
    if (PlayoutControllerInfo[9].Connected) then AirChainConnectLED_9.Lit := TRUE
    else AirChainConnectLED_9.Lit := FALSE;
    if (PlayoutControllerInfo[9].ErrorFlag) then AirChainStatusLED_9.Lit := TRUE
    else AirChainStatusLED_9.Lit := FALSE;
  end;
  //Do air-chain #10
  if (PlayoutControllerInfo[10].ControllerActive) then
  begin
    if (PlayoutControllerInfo[10].Connected) then AirChainConnectLED_10.Lit := TRUE
    else AirChainConnectLED_10.Lit := FALSE;
    if (PlayoutControllerInfo[10].ErrorFlag) then AirChainStatusLED_10.Lit := TRUE
    else AirChainStatusLED_10.Lit := FALSE;
  end;
  //Do air-chain #11
  if (PlayoutControllerInfo[11].ControllerActive) then
  begin
    if (PlayoutControllerInfo[11].Connected) then AirChainConnectLED_11.Lit := TRUE
    else AirChainConnectLED_11.Lit := FALSE;
    if (PlayoutControllerInfo[11].ErrorFlag) then AirChainStatusLED_11.Lit := TRUE
    else AirChainStatusLED_11.Lit := FALSE;
  end;
  //Do air-chain #12
  if (PlayoutControllerInfo[12].ControllerActive) then
  begin
    if (PlayoutControllerInfo[12].Connected) then AirChainConnectLED_12.Lit := TRUE
    else AirChainConnectLED_12.Lit := FALSE;
    if (PlayoutControllerInfo[12].ErrorFlag) then AirChainStatusLED_12.Lit := TRUE
    else AirChainStatusLED_12.Lit := FALSE;
  end;
  //Do air-chain #13
  if (PlayoutControllerInfo[13].ControllerActive) then
  begin
    if (PlayoutControllerInfo[13].Connected) then AirChainConnectLED_13.Lit := TRUE
    else AirChainConnectLED_13.Lit := FALSE;
    if (PlayoutControllerInfo[13].ErrorFlag) then AirChainStatusLED_13.Lit := TRUE
    else AirChainStatusLED_13.Lit := FALSE;
  end;
  //Do air-chain #14
  if (PlayoutControllerInfo[14].ControllerActive) then
  begin
    if (PlayoutControllerInfo[14].Connected) then AirChainConnectLED_14.Lit := TRUE
    else AirChainConnectLED_14.Lit := FALSE;
    if (PlayoutControllerInfo[14].ErrorFlag) then AirChainStatusLED_14.Lit := TRUE
    else AirChainStatusLED_14.Lit := FALSE;
  end;
  //Do air-chain #15
  if (PlayoutControllerInfo[15].ControllerActive) then
  begin
    if (PlayoutControllerInfo[15].Connected) then AirChainConnectLED_15.Lit := TRUE
    else AirChainConnectLED_15.Lit := FALSE;
    if (PlayoutControllerInfo[15].ErrorFlag) then AirChainStatusLED_15.Lit := TRUE
    else AirChainStatusLED_15.Lit := FALSE;
  end;
  //Do air-chain #16
  if (PlayoutControllerInfo[16].ControllerActive) then
  begin
    if (PlayoutControllerInfo[16].Connected) then AirChainConnectLED_16.Lit := TRUE
    else AirChainConnectLED_16.Lit := FALSE;
    if (PlayoutControllerInfo[16].ErrorFlag) then AirChainStatusLED_16.Lit := TRUE
    else AirChainStatusLED_16.Lit := FALSE;
  end;
  //Do air-chain #17
  if (PlayoutControllerInfo[17].ControllerActive) then
  begin
    if (PlayoutControllerInfo[17].Connected) then AirChainConnectLED_17.Lit := TRUE
    else AirChainConnectLED_17.Lit := FALSE;
    if (PlayoutControllerInfo[17].ErrorFlag) then AirChainStatusLED_17.Lit := TRUE
    else AirChainStatusLED_17.Lit := FALSE;
  end;
  //Do air-chain #18
  if (PlayoutControllerInfo[18].ControllerActive) then
  begin
    if (PlayoutControllerInfo[18].Connected) then AirChainConnectLED_18.Lit := TRUE
    else AirChainConnectLED_18.Lit := FALSE;
    if (PlayoutControllerInfo[18].ErrorFlag) then AirChainStatusLED_18.Lit := TRUE
    else AirChainStatusLED_18.Lit := FALSE;
  end;
  //Do air-chain #19
  if (PlayoutControllerInfo[19].ControllerActive) then
  begin
    if (PlayoutControllerInfo[19].Connected) then AirChainConnectLED_19.Lit := TRUE
    else AirChainConnectLED_19.Lit := FALSE;
    if (PlayoutControllerInfo[19].ErrorFlag) then AirChainStatusLED_19.Lit := TRUE
    else AirChainStatusLED_19.Lit := FALSE;
  end;
  //Do air-chain #20
  if (PlayoutControllerInfo[20].ControllerActive) then
  begin
    if (PlayoutControllerInfo[20].Connected) then AirChainConnectLED_20.Lit := TRUE
    else AirChainConnectLED_20.Lit := FALSE;
    if (PlayoutControllerInfo[20].ErrorFlag) then AirChainStatusLED_20.Lit := TRUE
    else AirChainStatusLED_20.Lit := FALSE;
  end;
end;

//Procedure to try to connect to all of the enabled controllers - called on main timer
procedure TMainForm.ConnectToControllers;
begin
  try
    //Init connections to Ticker Playout Controllers
    //Do Playout Controller #1
    if not (PlayoutInterface.PlayoutConnection1.Active) then
    begin
      if (PlayoutControllerInfo[1].Enabled) AND (PlayoutControllerInfo[1].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection1.Address := PlayoutControllerInfo[1].IPAddress;
        PlayoutInterface.PlayoutConnection1.Port := PlayoutControllerInfo[1].Port;
        PlayoutInterface.PlayoutConnection1.Active := TRUE;
      end;
    end;

    //Do Playout Controller #2
    if not (PlayoutInterface.PlayoutConnection2.Active) then
    begin
      if (PlayoutControllerInfo[2].Enabled) AND (PlayoutControllerInfo[2].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection2.Address := PlayoutControllerInfo[2].IPAddress;
        PlayoutInterface.PlayoutConnection2.Port := PlayoutControllerInfo[2].Port;
        PlayoutInterface.PlayoutConnection2.Active := TRUE;
      end;
    end;

    //Do Playout Controller #3
    if not (PlayoutInterface.PlayoutConnection3.Active) then
    begin
      if (PlayoutControllerInfo[3].Enabled) AND (PlayoutControllerInfo[3].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection3.Address := PlayoutControllerInfo[3].IPAddress;
        PlayoutInterface.PlayoutConnection3.Port := PlayoutControllerInfo[3].Port;
        PlayoutInterface.PlayoutConnection3.Active := TRUE;
      end;
    end;

    //Do Playout Controller #4
    if not (PlayoutInterface.PlayoutConnection4.Active) then
    begin
      if (PlayoutControllerInfo[4].Enabled) AND (PlayoutControllerInfo[4].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection4.Address := PlayoutControllerInfo[4].IPAddress;
        PlayoutInterface.PlayoutConnection4.Port := PlayoutControllerInfo[4].Port;
        PlayoutInterface.PlayoutConnection4.Active := TRUE;
      end;
    end;

    //Do Playout Controller #5
    if not (PlayoutInterface.PlayoutConnection5.Active) then
    begin
      if (PlayoutControllerInfo[5].Enabled) AND (PlayoutControllerInfo[5].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection5.Address := PlayoutControllerInfo[5].IPAddress;
        PlayoutInterface.PlayoutConnection5.Port := PlayoutControllerInfo[5].Port;
        PlayoutInterface.PlayoutConnection5.Active := TRUE;
      end;
    end;

    //Do Playout Controller #6
    if not (PlayoutInterface.PlayoutConnection6.Active) then
    begin
      if (PlayoutControllerInfo[6].Enabled) AND (PlayoutControllerInfo[6].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection6.Address := PlayoutControllerInfo[6].IPAddress;
        PlayoutInterface.PlayoutConnection6.Port := PlayoutControllerInfo[6].Port;
        PlayoutInterface.PlayoutConnection6.Active := TRUE;
      end;
    end;

    //Do Playout Controller #7
    if not (PlayoutInterface.PlayoutConnection7.Active) then
    begin
      if (PlayoutControllerInfo[7].Enabled) AND (PlayoutControllerInfo[7].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection7.Address := PlayoutControllerInfo[7].IPAddress;
        PlayoutInterface.PlayoutConnection7.Port := PlayoutControllerInfo[7].Port;
        PlayoutInterface.PlayoutConnection7.Active := TRUE;
      end;
    end;

    //Do Playout Controller #8
    if not (PlayoutInterface.PlayoutConnection8.Active) then
    begin
      if (PlayoutControllerInfo[8].Enabled) AND (PlayoutControllerInfo[8].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection8.Address := PlayoutControllerInfo[8].IPAddress;
        PlayoutInterface.PlayoutConnection8.Port := PlayoutControllerInfo[8].Port;
        PlayoutInterface.PlayoutConnection8.Active := TRUE;
      end;
    end;

    //Do Playout Controller #9
    if not (PlayoutInterface.PlayoutConnection9.Active) then
    begin
      if (PlayoutControllerInfo[9].Enabled) AND (PlayoutControllerInfo[9].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection9.Address := PlayoutControllerInfo[9].IPAddress;
        PlayoutInterface.PlayoutConnection9.Port := PlayoutControllerInfo[9].Port;
        PlayoutInterface.PlayoutConnection9.Active := TRUE;
      end;
    end;

    //Do Playout Controller #10
    if not (PlayoutInterface.PlayoutConnection10.Active) then
    begin
      if (PlayoutControllerInfo[10].Enabled) AND (PlayoutControllerInfo[10].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection10.Address := PlayoutControllerInfo[10].IPAddress;
        PlayoutInterface.PlayoutConnection10.Port := PlayoutControllerInfo[10].Port;
        PlayoutInterface.PlayoutConnection10.Active := TRUE;
      end;
    end;

    //Do Playout Controller #11
    if not (PlayoutInterface.PlayoutConnection11.Active) then
    begin
      if (PlayoutControllerInfo[11].Enabled) AND (PlayoutControllerInfo[11].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection11.Address := PlayoutControllerInfo[11].IPAddress;
        PlayoutInterface.PlayoutConnection11.Port := PlayoutControllerInfo[11].Port;
        PlayoutInterface.PlayoutConnection11.Active := TRUE;
      end;
    end;

    //Do Playout Controller #12
    if not (PlayoutInterface.PlayoutConnection12.Active) then
    begin
      if (PlayoutControllerInfo[12].Enabled) AND (PlayoutControllerInfo[12].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection12.Address := PlayoutControllerInfo[12].IPAddress;
        PlayoutInterface.PlayoutConnection12.Port := PlayoutControllerInfo[12].Port;
        PlayoutInterface.PlayoutConnection12.Active := TRUE;
      end;
    end;

    //Do Playout Controller #13
    if not (PlayoutInterface.PlayoutConnection13.Active) then
    begin
      if (PlayoutControllerInfo[13].Enabled) AND (PlayoutControllerInfo[13].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection13.Address := PlayoutControllerInfo[13].IPAddress;
        PlayoutInterface.PlayoutConnection13.Port := PlayoutControllerInfo[13].Port;
        PlayoutInterface.PlayoutConnection13.Active := TRUE;
      end;
    end;

    //Do Playout Controller #14
    if not (PlayoutInterface.PlayoutConnection14.Active) then
    begin
      if (PlayoutControllerInfo[14].Enabled) AND (PlayoutControllerInfo[14].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection14.Address := PlayoutControllerInfo[14].IPAddress;
        PlayoutInterface.PlayoutConnection14.Port := PlayoutControllerInfo[14].Port;
        PlayoutInterface.PlayoutConnection14.Active := TRUE;
      end;
    end;

    //Do Playout Controller #15
    if not (PlayoutInterface.PlayoutConnection15.Active) then
    begin
      if (PlayoutControllerInfo[15].Enabled) AND (PlayoutControllerInfo[15].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection15.Address := PlayoutControllerInfo[15].IPAddress;
        PlayoutInterface.PlayoutConnection15.Port := PlayoutControllerInfo[15].Port;
        PlayoutInterface.PlayoutConnection15.Active := TRUE;
      end;
    end;

    //Do Playout Controller #16
    if not (PlayoutInterface.PlayoutConnection16.Active) then
    begin
      if (PlayoutControllerInfo[16].Enabled) AND (PlayoutControllerInfo[16].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection16.Address := PlayoutControllerInfo[16].IPAddress;
        PlayoutInterface.PlayoutConnection16.Port := PlayoutControllerInfo[16].Port;
        PlayoutInterface.PlayoutConnection16.Active := TRUE;
      end;
    end;

    //Do Playout Controller #17
    if not (PlayoutInterface.PlayoutConnection17.Active) then
    begin
      if (PlayoutControllerInfo[17].Enabled) AND (PlayoutControllerInfo[17].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection17.Address := PlayoutControllerInfo[17].IPAddress;
        PlayoutInterface.PlayoutConnection17.Port := PlayoutControllerInfo[17].Port;
        PlayoutInterface.PlayoutConnection17.Active := TRUE;
      end;
    end;

    //Do Playout Controller #18
    if not (PlayoutInterface.PlayoutConnection18.Active) then
    begin
      if (PlayoutControllerInfo[18].Enabled) AND (PlayoutControllerInfo[18].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection18.Address := PlayoutControllerInfo[18].IPAddress;
        PlayoutInterface.PlayoutConnection18.Port := PlayoutControllerInfo[18].Port;
        PlayoutInterface.PlayoutConnection18.Active := TRUE;
      end;
    end;

    //Do Playout Controller #19
    if not (PlayoutInterface.PlayoutConnection19.Active) then
    begin
      if (PlayoutControllerInfo[19].Enabled) AND (PlayoutControllerInfo[19].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection19.Address := PlayoutControllerInfo[19].IPAddress;
        PlayoutInterface.PlayoutConnection19.Port := PlayoutControllerInfo[19].Port;
        PlayoutInterface.PlayoutConnection19.Active := TRUE;
      end;
    end;

    //Do Playout Controller #20
    if not (PlayoutInterface.PlayoutConnection20.Active) then
    begin
      if (PlayoutControllerInfo[20].Enabled) AND (PlayoutControllerInfo[20].EnableForMasterControl) then
      begin
        PlayoutInterface.PlayoutConnection20.Address := PlayoutControllerInfo[20].IPAddress;
        PlayoutInterface.PlayoutConnection20.Port := PlayoutControllerInfo[20].Port;
        PlayoutInterface.PlayoutConnection20.Active := TRUE;
      end;
    end;
  except
    MessageDlg('Error occurred while trying to connect to Playout Controllers!', mtError, [mbOK], 0);
  end;
end;

//Handler for time of day timer
procedure TMainForm.timeOfDayTimerTimer(Sender: TObject);
begin
  //Update date/time labels
  TimeLabel.Caption := TimeToStr(Now);
  DateLabel.Caption := DateToStr(Now);

  //Update status of all air-chains
  UpdateStatusAllAirChains;

  //Connect to the controllers
  ConnectToControllers;

  //Update controls on main control panel
  UpdateMainControlPanel;
end;

//Handler for change in network selection
procedure TMainForm.NetworkSelectDropDownChange(Sender: TObject);
begin
  ProcessNetworkSelectChange;
end;

procedure TMainForm.ProcessNetworkSelectChange;
var
  NetworkIndex: SmallInt;
begin
  //Update the control panel settings
  UpdateMainControlPanel;

  //Setup the database connections to the applicable network
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;  //Setup data source
  try
    with dmMain.dbNetwork do
    begin
      Connected := FALSE;
      ConnectionString := NetworkInfo[NetworkIndex].DBConnectionString;
      Connected := TRUE;
    end;
  except
    //Log error
    MessageDlg('Error occurred while trying to connect to switch networks!', mtError, [mbOK], 0);
  end;

  //Update the error log
  UpdateErrorLog;

  //Update the As-Run log
  UpdateAsRunLog;
end;

//Function to update As-Run log
procedure TMainForm.UpdateAsRunLog;
var
  NetworkIndex: SmallInt;
  StationID: SmallInt;
begin
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;  //Setup data source

  //Don't allow invalid condition for DB select
  //Set radio buton accordingly if only one DB connected
  if (dmMain.dbMasterControl_X.Connected) and not (dmMain.dbMasterControl_Y.Connected) then
  begin
    AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_X;
  end
  else if (dmMain.dbMasterControl_Y.Connected) and not (dmMain.dbMasterControl_X.Connected) then
  begin
    AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_Y;
  end;

  //Get station ID
  Case AirChainSelectAsRunLog.ItemIndex of
    0: begin
         dmMain.AsRunQuery.Connection := dmMain.dbMasterControl_X;
         StationID := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].StationID;
       end;
    1: begin
         dmMain.AsRunQuery.Connection := dmMain.dbMasterControl_Y;
         StationID := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].StationID;
       end;
  end;
  //Do As-Run log query
  try
    with dmMain.AsRunQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM AsRunLog WHERE StationID = ' + IntToStr(StationID));
      Open;
    end;
  except
    //Log error
  end;
end;

//Function to update error log
procedure TMainForm.UpdateErrorLog;
var
  NetworkIndex: SmallInt;
  StationID: SmallInt;
begin
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;  //Setup data source

  //Set radio buton accordingly if only one DB connected
  if (dmMain.dbMasterControl_X.Connected) and not (dmMain.dbMasterControl_Y.Connected) then
  begin
    AirChainSelectErrorLog.ItemIndex := LOG_SELECT_X;
  end
  else if (dmMain.dbMasterControl_Y.Connected) and not (dmMain.dbMasterControl_X.Connected) then
  begin
    AirChainSelectErrorLog.ItemIndex := LOG_SELECT_Y;
  end;

  //Get station ID
  Case AirChainSelectErrorLog.ItemIndex of
    0: begin
         dmMain.ErrorQuery.Connection := dmMain.dbMasterControl_X;
         StationID := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].StationID;
       end;
    1: begin
         dmMain.ErrorQuery.Connection := dmMain.dbMasterControl_Y;
         StationID := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].StationID;
       end;
  end;
  //Do error log query
  try
    with dmMain.ErrorQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM ErrorLog WHERE StationID = ' + IntToStr(StationID));
      Open;
    end;
  except
    //Log error
  end;
end;

//Pocedure to update the main status panel based on the selected network
procedure TMainForm.UpdateMainControlPanel;
var
  NetworkIndex: SmallInt;
begin
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;

  //Update controls
  //Do playlist names
  //Do X side
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) and
    ((AirChainSelectRadioBtn.ItemIndex = X_AND_Y) or (AirChainSelectRadioBtn.ItemIndex = X_ONLY)) then
    CurrentPlaylistName_X.Caption := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].CurrentPlaylistName
  else
    CurrentPlaylistName_X.Caption := 'N/A';
  //Do Y side
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) and
     ((AirChainSelectRadioBtn.ItemIndex = X_AND_Y) or (AirChainSelectRadioBtn.ItemIndex = Y_ONLY)) then
    CurrentPlaylistName_Y.Caption := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].CurrentPlaylistName
  else
    CurrentPlaylistName_Y.Caption := 'N/A';

  //Do error condition
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
    AirChainErrorLED_X.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].ErrorFlag
  else
    AirChainErrorLED_X.Lit := FALSE;
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
    AirChainErrorLED_Y.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].ErrorFlag
  else
    AirChainErrorLED_Y.Lit := FALSE;

  //Do graphics engine status
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
    GraphicsEngineLED_X.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].TickerEngineOK
  else
    GraphicsEngineLED_X.Lit := FALSE;
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
    GraphicsEngineLED_Y.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].TickerEngineOK
  else
    GraphicsEngineLED_Y.Lit := FALSE;

  //Do Ticker Running status
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
    TickerRunningLED_X.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].TickerRunning
  else
    TickerRunningLED_X.Lit := FALSE;
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
    TickerRunningLED_Y.Lit := PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].TickerRunning
  else
    TickerRunningLED_Y.Lit := FALSE;

  //Do Schedule/manual mode status
  //Do X Side
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) and
    ((AirChainSelectRadioBtn.ItemIndex = X_AND_Y) or (AirChainSelectRadioBtn.ItemIndex = X_ONLY)) then
  begin
    if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].InManualMode) then
    begin
      ScheduleModeLED_X.Lit := FALSE;
      ManualModeLED_X.Lit := TRUE;
      //if (UseXAsMainDBServer) then
      begin
        SchedulingModeBtn.Caption := 'Go to Scheduled Playlist Mode';
        SelectPlaylistBtn.Visible := TRUE;
      end;
    end
    else begin
      ScheduleModeLED_X.Lit := TRUE;
      ManualModeLED_X.Lit := FALSE;
      //if (UseXAsMainDBServer) then
      begin
        SchedulingModeBtn.Caption := 'Go to Manual Playlist Mode';
        SelectPlaylistBtn.Visible := FALSE;
      end;
    end;
  end
  else begin
    ScheduleModeLED_X.Lit := FALSE;
    ManualModeLED_X.Lit := FALSE;
  end;

  //Do Y Side
  //Check for X and Y selected
  if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) and
     (AirChainSelectRadioBtn.ItemIndex = X_AND_Y) then
  begin
    if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].InManualMode) then
    begin
      ScheduleModeLED_Y.Lit := FALSE;
      ManualModeLED_Y.Lit := TRUE;
    end
    else begin
      ScheduleModeLED_Y.Lit := TRUE;
      ManualModeLED_Y.Lit := FALSE;
    end;
  end
  //Check for Y_ONLY selected
  else if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) and
     (AirChainSelectRadioBtn.ItemIndex = Y_ONLY) then
  begin
    if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].InManualMode) then
    begin
      ScheduleModeLED_Y.Lit := FALSE;
      ManualModeLED_Y.Lit := TRUE;
      //if not (UseXAsMainDBServer) then
      begin
        SchedulingModeBtn.Caption := 'Go to Scheduled Playlist Mode';
        SelectPlaylistBtn.Visible := TRUE;
      end;
    end
    else begin
      ScheduleModeLED_Y.Lit := TRUE;
      ManualModeLED_Y.Lit := FALSE;
      //if not (UseXAsMainDBServer) then
      begin
        SchedulingModeBtn.Caption := 'Go to Manual Playlist Mode';
        SelectPlaylistBtn.Visible := FALSE;
      end;
    end;
  end
  else begin
    ScheduleModeLED_Y.Lit := FALSE;
    ManualModeLED_Y.Lit := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// REMOTE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//General command to send a command to the controller
procedure TMainForm.SendCommandToController(CommandStr: String);
var
  NetworkIndex: SmallInt;
begin
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;
  //Do X-Side
  if (AirChainSelectRadioBtn.ItemIndex = X_AND_Y) OR (AirChainSelectRadioBtn.ItemIndex = X_ONLY) then
  begin
    Case NetworkIndex of
      1: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection1.Socket.SendText(CommandStr);
      2: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection3.Socket.SendText(CommandStr);
      3: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection5.Socket.SendText(CommandStr);
      4: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection7.Socket.SendText(CommandStr);
      5: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection9.Socket.SendText(CommandStr);
      6: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection11.Socket.SendText(CommandStr);
      7: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection13.Socket.SendText(CommandStr);
      8: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection15.Socket.SendText(CommandStr);
      9: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection17.Socket.SendText(CommandStr);
     10: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
        PlayoutInterface.PlayoutConnection19.Socket.SendText(CommandStr);
    end;
  end;
  //Do Y-Side
  if (AirChainSelectRadioBtn.ItemIndex = X_AND_Y) OR (AirChainSelectRadioBtn.ItemIndex = Y_ONLY) then
  begin
    Case NetworkIndex of
      1: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection2.Socket.SendText(CommandStr);
      2: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection4.Socket.SendText(CommandStr);
      3: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection6.Socket.SendText(CommandStr);
      4: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection8.Socket.SendText(CommandStr);
      5: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection10.Socket.SendText(CommandStr);
      6: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection12.Socket.SendText(CommandStr);
      7: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection14.Socket.SendText(CommandStr);
      8: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection16.Socket.SendText(CommandStr);
      9: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection18.Socket.SendText(CommandStr);
     10: if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
        PlayoutInterface.PlayoutConnection20.Socket.SendText(CommandStr);
    end;
  end;
end;

//Function to switch between manual/scheduled mode
procedure TMainForm.SchedulingModeBtnClick(Sender: TObject);
var
  CmdStr: String;
  NetworkIndex: SmallInt;
begin
  NetworkIndex := NetworkSelectDropDown.ItemIndex+1;
  Case AirChainSelectRadioBtn.ItemIndex of
    X_AND_Y, X_ONLY: begin
      //Build command string
      if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].Connected) then
      begin
        //If in local mode, switch to schedule mode
        if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 1].InManualMode) then
        begin
          CmdStr := SOH;
          CmdStr := CmdStr + 'SET_LOCAL_MODE' + ETX + IntToStr(SCHEDULE_MODE) + ETX + ' ';
          CmdStr := CmdStr + EOT;
        end
        else begin
          CmdStr := SOH;
          CmdStr := CmdStr + 'SET_LOCAL_MODE' + ETX + IntToStr(MANUAL_MODE) + ETX + ' ';
          CmdStr := CmdStr + EOT;
        end;
      end;
    end;
    //If Y-only, use setting of Y-Air Chain as guid
    Y_ONLY: begin
      //Build command string
      if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].Connected) then
      begin
        //If in local mode, switch to schedule mode
        if (PlayoutControllerInfo[((NetworkIndex-1)*2) + 2].InManualMode) then
        begin
          CmdStr := SOH;
          CmdStr := CmdStr + 'SET_LOCAL_MODE' + ETX + IntToStr(SCHEDULE_MODE) + ETX + ' ';
          CmdStr := CmdStr + EOT;
        end
        else begin
          CmdStr := SOH;
          CmdStr := CmdStr + 'SET_LOCAL_MODE' + ETX + IntToStr(MANUAL_MODE) + ETX + ' ';
          CmdStr := CmdStr + EOT;
        end;
      end;
    end;
  end;
  //Call function to send command
  SendCommandToController(CmdStr);
end;

//Function to trigger ticker
procedure TMainForm.TriggerPlaylistBtnClick(Sender: TObject);
var
  CmdStr: String;
begin
  //Build command string
  CmdStr := SOH;
  CmdStr := CmdStr + 'START' + ETX + ' ' + ETX + ' ';
  CmdStr := CmdStr + EOT;
  //Call function to send command
  SendCommandToController(CmdStr);
end;

//Function to abort ticker
procedure TMainForm.PausePlaylistBtnClick(Sender: TObject);
var
  CmdStr: String;
begin
  //Build command string
  CmdStr := SOH;
  CmdStr := CmdStr + 'STOP' + ETX + ' ' + ETX + ' ';
  CmdStr := CmdStr + EOT;
  //Call function to send command
  SendCommandToController(CmdStr);
end;

//Function to reset ticker to top
procedure TMainForm.ResetPlaylistBtnClick(Sender: TObject);
var
  CmdStr: String;
begin
  //Build command string
  CmdStr := SOH;
  CmdStr := CmdStr + 'RESET_TICKER' + ETX + ' ' + ETX + ' ';
  CmdStr := CmdStr + EOT;
  //Call function to send command
  SendCommandToController(CmdStr);
end;

//Function to reset ticker error
procedure TMainForm.ResetErrorBtnClick(Sender: TObject);
var
  CmdStr: String;
begin
  //Build command string
  CmdStr := SOH;
  CmdStr := CmdStr + 'RESET_ERROR' + ETX + ' ' + ETX + ' ';
  CmdStr := CmdStr + EOT;
  //Call function to send command
  SendCommandToController(CmdStr);
end;

//Handler for select playlist to load
procedure TMainForm.SelectPlaylistBtnClick(Sender: TObject);
begin
  try
    with dmMain.PlaylistQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM Ticker_Groups');
      Open;
    end;
  except
    //Log error
  end;
  //Show playlist select dialog
  PlaylistSelectPanel.Visible := TRUE;
end;

//Handler to confirm playlist load
procedure TMainForm.LoadPlaylistBtnClick(Sender: TObject);
var
  CmdStr: String;
  PlaylistIDStr: String;
begin
  //Send command to load playlist
  //Get playlist ID
  PlaylistIDStr := dmMain.PlaylistQuery.FieldByName('Playlist_ID').AsString;
  //Build command string
  CmdStr := SOH;
  CmdStr := CmdStr + 'LOAD' + ETX + PlaylistIDStr + ETX + ' ';
  CmdStr := CmdStr + EOT;
  //Call function to send command
  SendCommandToController(CmdStr);

  //Close query
  dmMain.PlaylistQuery.Close;

  //Hide playlist panel
  PlaylistSelectPanel.Visible := FALSE;
end;

//Handler for double-click on grid to select playlist
procedure TMainForm.AvailablePlaylistGridDblClick(Sender: TObject);
begin
  LoadPlaylistBtn.Click();
end;

//Handler to cancel playlist load
procedure TMainForm.CancelPlaylistLoadBtnClick(Sender: TObject);
begin
  //Hide playlist panel
  PlaylistSelectPanel.Visible := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to view As-Run log
procedure TMainForm.ViewAsRunLogBtnClick(Sender: TObject);
begin
  AsRunLogViewerPnl.Visible := TRUE;
end;

//Handler to hide As-Run log viewer
procedure TMainForm.CloseAsRunLogBtnClick(Sender: TObject);
begin
  AsRunLogViewerPnl.Visible := FALSE;
end;

//Handler to refresh As-Run log
procedure TMainForm.RefreshAsRunLogBtnClick(Sender: TObject);
begin
  UpdateAsRunLog;
end;

//Handler for As-Run X/Y change
procedure TMainForm.AirChainSelectAsRunLogClick(Sender: TObject);
begin
  UpdateAsRunLog;
end;

//Handler to display log viewer
procedure TMainForm.ViewErrorLogBtnClick(Sender: TObject);
begin
  ErrorLogViewerPnl.Visible := TRUE;
end;

//Handler to close log viewer
procedure TMainForm.CloseErrorLogBtnClick(Sender: TObject);
begin
  ErrorLogViewerPnl.Visible := FALSE;
end;

//Handler for refresh error log
procedure TMainForm.RefreshErrorLogBtnClick(Sender: TObject);
begin
  UpdateErrorLog;
end;

//Handler for error log X/Y change
procedure TMainForm.AirChainSelectErrorLogClick(Sender: TObject);
begin
  UpdateErrorLog;
end;

////////////////////////////////////////////////////////////////////////////////
// DATABASE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for X-side server enable
procedure TMainForm.XSideServerEnabledClick(Sender: TObject);
begin
  if (DBServerXEnabled) then
  begin
    DBServerXEnabled := FALSE;
    XSideServerEnabled.Checked := FALSE;
    dmMain.dbMasterControl_X.Connected := FALSE;
    DatabaseConnectLED_X.Lit := FALSE;
    DatabaseDisconnectLED_X.Lit := TRUE;

    //Switch to backup if needed
    if (UseXAsMainDBServer) then
    begin
      UseXAsMainDBServer := FALSE;
      UseXSideServerasMaster.Checked := FALSE;
      UseYSideServerasMaster.Checked := TRUE;
      YDBLabel.Visible := TRUE;
    end;
  end
  else begin
    DBServerXEnabled := TRUE;
    XSideServerEnabled.Checked := TRUE;
    try
      XSideServerEnabled.Checked := TRUE;
      With dmMain do
      begin
        dbMasterControl_X.Connected := FALSE;
        dbMasterControl_X.ConnectionString := DBConnectionString_X;
        dbMasterControl_X.Connected := TRUE;
        //Set indicators
        if (dbMasterControl_X.Connected) then
        begin
          DatabaseConnectLED_X.Lit := TRUE;
          DatabaseDisconnectLED_X.Lit := FALSE;
        end
        else begin
          DatabaseConnectLED_X.Lit := FALSE;
          DatabaseDisconnectLED_X.Lit := TRUE;
        end;
      end;
    except
      //Set indicators
      XSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_X.Lit := TRUE;
      DatabaseConnectLED_X.Lit := FALSE;
    end;
  end;
end;

//Handler for Y-side server enable
procedure TMainForm.YSideServerEnabledClick(Sender: TObject);
begin
  if (DBServerYEnabled) then
  begin
    DBServerYEnabled := FALSE;
    YSideServerEnabled.Checked := FALSE;
    dmMain.dbMasterControl_Y.Connected := FALSE;
    DatabaseConnectLED_Y.Lit := FALSE;
    DatabaseDisconnectLED_Y.Lit := TRUE;

    //Switch to primary if needed
    if not (UseXAsMainDBServer) then
    begin
      UseXAsMainDBServer := TRUE;
      UseYSideServerasMaster.Checked := FALSE;
      UseXSideServerasMaster.Checked := TRUE;
      YDBLabel.Visible := FALSE;
    end;
  end
  else begin
    DBServerYEnabled := TRUE;
    YSideServerEnabled.Checked := TRUE;
    try
      With dmMain do
      begin
        dbMasterControl_Y.Connected := FALSE;
        dbMasterControl_Y.ConnectionString := DBConnectionString_Y;
        dbMasterControl_Y.Connected := TRUE;
        //Set indicators
        if (dbMasterControl_Y.Connected) then
        begin
          DatabaseConnectLED_Y.Lit := TRUE;
          DatabaseDisconnectLED_Y.Lit := FALSE;
        end
        else begin
          DatabaseConnectLED_Y.Lit := FALSE;
          DatabaseDisconnectLED_Y.Lit := TRUE;
        end;
      end;
    except
      //Set indicators
      YSideServerEnabled.Checked := FALSE;
      DatabaseDisconnectLED_Y.Lit := TRUE;
      DatabaseConnectLED_Y.Lit := FALSE;
    end;
  end;
end;

//Handler for use X-server as master
procedure TMainForm.UseXSideServerasMasterClick(Sender: TObject);
begin
  if (dmMain.dbMasterControl_X.Connected) then
  begin
    UseXAsMainDBServer := TRUE;
    UseXSideServerasMaster.Checked := TRUE;
    UseYSideServerasMaster.Checked := FALSE;
    YDBLabel.Visible := FALSE;
  end;
end;

//Handler for use Y-server as master
procedure TMainForm.UseYSideServerasMasterClick(Sender: TObject);
begin
  if (dmMain.dbMasterControl_Y.Connected) then
  begin
    UseXAsMainDBServer := FALSE;
    UseXSideServerasMaster.Checked := FALSE;
    UseYSideServerasMaster.Checked := TRUE;
    YDBLabel.Visible := TRUE;
  end;
end;

//Handler for attempt to change air-chain select radio button
procedure TMainForm.AirChainSelectRadioBtnClick(Sender: TObject);
begin
  //Set radio buton accordingly if only one DB connected
  if (dmMain.dbMasterControl_X.Connected) and not (dmMain.dbMasterControl_Y.Connected) then
  begin
//    AirChainSelectRadioBtn.ItemIndex := X_ONLY;
  end
  else if (dmMain.dbMasterControl_Y.Connected) and not (dmMain.dbMasterControl_X.Connected) then
  begin
//    AirChainSelectRadioBtn.ItemIndex := Y_ONLY;
  end;
end;

//Handler for re-connect to databases menu selection
procedure TMainForm.R1Click(Sender: TObject);
begin
  //Set state of DB servers
  if (UseXAsMainDBServer) then
  begin
    UseXSideServerasMaster.Checked := TRUE;
    UseYSideServerasMaster.Checked := FALSE;
    YDBLabel.Visible := FALSE;
  end
  else begin
    UseXSideServerasMaster.Checked := FALSE;
    UseYSideServerasMaster.Checked := TRUE;
    YDBLabel.Visible := TRUE;
  end;

  //Connect to X Database
  if (DBServerXEnabled) then
  try
    XSideServerEnabled.Checked := TRUE;
    With dmMain do
    begin
      dbMasterControl_X.Connected := FALSE;
      dbMasterControl_X.ConnectionString := DBConnectionString_X;
      dbMasterControl_X.Connected := TRUE;
      //Set indicators
      if (dbMasterControl_X.Connected) then
      begin
        DatabaseConnectLED_X.Lit := TRUE;
        DatabaseDisconnectLED_X.Lit := FALSE;
      end
      else begin
        DatabaseConnectLED_X.Lit := FALSE;
        DatabaseDisconnectLED_X.Lit := TRUE;
      end;
    end;
  except
    //Set indicators
    XSideServerEnabled.Checked := FALSE;
    DatabaseDisconnectLED_X.Lit := TRUE;
    DatabaseConnectLED_X.Lit := FALSE;
  end
  else XSideServerEnabled.Checked := FALSE;

  //Connect to Y Database
  if (DBServerYEnabled) then
  try
    YSideServerEnabled.Checked := TRUE;
    With dmMain do
    begin
      dbMasterControl_Y.Connected := FALSE;
      dbMasterControl_Y.ConnectionString := DBConnectionString_Y;
      dbMasterControl_Y.Connected := TRUE;
      //Set indicators
      if (dbMasterControl_Y.Connected) then
      begin
        DatabaseConnectLED_Y.Lit := TRUE;
        DatabaseDisconnectLED_Y.Lit := FALSE;
      end
      else begin
        DatabaseConnectLED_Y.Lit := FALSE;
        DatabaseDisconnectLED_Y.Lit := TRUE;
      end;
    end;
  except
    //Set indicators
    YSideServerEnabled.Checked := FALSE;
    DatabaseDisconnectLED_Y.Lit := TRUE;
    DatabaseConnectLED_Y.Lit := FALSE;
  end
  else YSideServerEnabled.Checked := FALSE;

  //Set radio buton accordingly if only one DB connected
  if (dmMain.dbMasterControl_X.Connected) and not (dmMain.dbMasterControl_Y.Connected) then
  begin
    AirChainSelectRadioBtn.ItemIndex := X_ONLY;
    AirChainSelectErrorLog.ItemIndex := LOG_SELECT_X;
    AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_X;
  end
  else if (dmMain.dbMasterControl_Y.Connected) and not (dmMain.dbMasterControl_X.Connected) then
  begin
    AirChainSelectRadioBtn.ItemIndex := Y_ONLY;
    AirChainSelectErrorLog.ItemIndex := LOG_SELECT_Y;
    AirChainSelectAsRunLog.ItemIndex := LOG_SELECT_Y;
  end;

  //Reload data
  ReloadDataFromDatabase;
end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TMainForm.WriteToErrorLog (ErrorString: String);
var
  FileName: String;
  DateStr: String;
  TimeStr: String;
  HoursStr: String[2];
  MinutesStr: String[2];
  SecondsStr: String[2];
  DayStr: String[2];
  MonthStr: String[2];
  TestStr: String;
  ErrorLogStr: String;
  Present: TDateTime;
  Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
  //Check for error log directory and create if it doesn't exist
  if (DirectoryExists('c:Comcast_NBC_Error_LogFiles') = FALSE) then
    CreateDir('c:\Comcast_NBC_Error_LogFiles');

  //Build date and time strings
  Present:= Now;
  DecodeTime (Present, Hour, Min, Sec, MSec);
  HoursStr := IntToStr(Hour);
  If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
  MinutesStr := IntToStr(Min);
  If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
  SecondsStr := IntToStr(Sec);
  If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
  TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
  DecodeDate (Present, Year, Month, Day);
  DayStr := IntToStr(Day);
  If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
  MonthStr := IntToStr(Month);
  If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
  DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
  ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;

  //Construct filename using current date - files are numbered 01 through 31
  FileName := 'c:\Comcast_NBC_Error_Logfiles\ErrorLog' + DayStr + '.txt';
  AssignFile (ErrorLogFile, FileName);
  //Write error information to one line in file
  //If file doesn't exist create a new one. Otherwise, check to see if it's one
  //month old. If so, delete & create a new one. Otherwise, append error to file
  if (Not (FileExists(FileName))) then
  begin
    ReWrite (ErrorLogFile);
    try
      Writeln (ErrorLogFile, ErrorLogStr);    finally
      CloseFile (ErrorLogFile);
    end;
  end
  else begin
    //Check file to see if it's a month old. If so, erase and create a new one
    Reset (ErrorLogFile);
    try
      Readln (ErrorLogFile, TestStr);
    finally
      CloseFile (ErrorLogFile);
    end;

    If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then
    begin      //Date in 1st entry in error log does not match today's date, so erase
      Erase (ErrorLogFile);
      //Create a new logfile
      ReWrite (ErrorLogFile);      try
        Writeln (ErrorLogFile, ErrorLogStr);      finally
        CloseFile (ErrorLogFile);
      end;
    end
    else begin
      //File exists and not a month old, so append to existing log file
      Append (ErrorLogFile);      try
        Writeln (ErrorLogFile, ErrorLogStr);      finally
        CloseFile (ErrorLogFile);
      end;
    end;
  end;
end;
end.

