Unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, {DBISAMTb,} {MSGParserServer,} ScktComp, ExtCtrls;

type
  ERecvError = class(Exception);

  TdmMain = class(TDataModule)
    dbMasterControl_X: TADOConnection;
    MCQuery: TADOQuery;
    dbMasterControl_Y: TADOConnection;
    PlaylistQuery: TADOQuery;
    dsPlaylistQuery: TDataSource;
    ErrorQuery: TADOQuery;
    dsErrorQuery: TDataSource;
    dbNetwork: TADOConnection;
    AsRunQuery: TADOQuery;
    dsAsRunQuery: TDataSource;
  private
  public
  end;

var
  dmMain: TdmMain;

implementation

uses Main;

{$R *.DFM}

end.
