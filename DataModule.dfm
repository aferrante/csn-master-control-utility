object dmMain: TdmMain
  OldCreateOrder = False
  Left = 623
  Top = 135
  Height = 392
  Width = 322
  object dbMasterControl_X: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 47
    Top = 30
  end
  object MCQuery: TADOQuery
    Connection = dbMasterControl_X
    Parameters = <>
    Left = 145
    Top = 78
  end
  object dbMasterControl_Y: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 47
    Top = 94
  end
  object PlaylistQuery: TADOQuery
    Connection = dbNetwork
    Parameters = <>
    Left = 129
    Top = 166
  end
  object dsPlaylistQuery: TDataSource
    DataSet = PlaylistQuery
    Left = 223
    Top = 167
  end
  object ErrorQuery: TADOQuery
    Connection = dbMasterControl_X
    Parameters = <>
    Left = 129
    Top = 222
  end
  object dsErrorQuery: TDataSource
    DataSet = ErrorQuery
    Left = 223
    Top = 223
  end
  object dbNetwork: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 47
    Top = 166
  end
  object AsRunQuery: TADOQuery
    Connection = dbMasterControl_X
    Parameters = <>
    Left = 129
    Top = 278
  end
  object dsAsRunQuery: TDataSource
    DataSet = AsRunQuery
    Left = 223
    Top = 279
  end
end
